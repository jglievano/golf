package golf

import (
	"testing"
)

func TestParamInitialization(t *testing.T) {
	r := NewRoute("", "/", nil)
	t.Run("base", func(t *testing.T) {
		if len(r.Params) > 0 {
			t.Fail()
		}
	})
	t.Run("samples", func(t *testing.T) {
		r.ChangePath("/samples")
		if len(r.Params) > 0 {
			t.Fail()
		}
	})
	t.Run("samples/:id", func(t *testing.T) {
		r.ChangePath("/samples/:id")
		if len(r.Params) != 1 {
			t.Fail()
		} else if r.Params[0] != ":id" {
			t.Fail()
		}
	})
	t.Run("samples/:id/edit", func(t *testing.T) {
		r.ChangePath("/samples/:id/edit")
		if len(r.Params) != 1 {
			t.Fail()
		} else if r.Params[0] != ":id" {
			t.Fail()
		}
	})
	t.Run("samples/:sample_id/edit/:id", func(t *testing.T) {
		r.ChangePath("/samples/:sample_id/edit/:id")
		if len(r.Params) != 2 {
			t.Fail()
		} else if r.Params[0] != ":sample_id" || r.Params[1] != ":id" {
			t.Fail()
		}
	})
}

func TestMatcherGeneration(t *testing.T) {
	r := NewRoute("", "/", nil)
	t.Run("base", func(t *testing.T) {
		if r.Matcher != "^/$" {
			t.Fail()
		}
	})
	t.Run("samples", func(t *testing.T) {
		r.ChangePath("/samples")
		if r.Matcher != "^/samples$" {
			t.Fail()
		}
	})
	t.Run("samples/:id", func(t *testing.T) {
		r.ChangePath("/samples/:id")
		if r.Matcher != "^/samples/[^/]+$" {
			t.Fail()
		}
	})
	t.Run("samples/:id/edit", func(t *testing.T) {
		r.ChangePath("/samples/:id/edit")
		if r.Matcher != "^/samples/[^/]+/edit$" {
			t.Fail()
		}
	})
	t.Run("samples/:sample_id/edit/:id", func(t *testing.T) {
		r.ChangePath("/samples/:sample_id/edit/:id")
		if r.Matcher != "^/samples/[^/]+/edit/[^/]+$" {
			t.Fail()
		}
	})
}
