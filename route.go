package golf

import (
	"net/http"
	"regexp"
	"strings"
)

var paramRe = regexp.MustCompile(":[^/]+")
var wildRe = regexp.MustCompile("\\*.+")

type Route struct {
	Path     string
	Method   string
	Params   []string
	Wildcard string
	Matcher  string
	Handler  func(*Handshake)
}

func NewRoute(method, path string, handler func(*Handshake)) (route *Route) {
	route = &Route{
		Path:    path,
		Method:  method,
		Handler: handler,
		Params:  paramRe.FindAllString(path, -1),
	}
	wildcards := wildRe.FindAllString(path, 1)
	if len(wildcards) > 0 {
		route.Wildcard = wildcards[0]
	}
	route.Matcher = route.GenerateMatcher(path)
	return
}

func (route *Route) ChangePath(path string) {
	route.Path = path
	route.Params = paramRe.FindAllString(path, -1)
	wildcards := wildRe.FindAllString(path, 1)
	if len(wildcards) > 0 {
		route.Wildcard = wildcards[0]
	}
	route.Matcher = route.GenerateMatcher(path)
}

func (route *Route) PathParams(path string) map[string]string {
	params := make(map[string]string)
	if route.Wildcard != "" {
		begin := strings.Index(route.Path, route.Wildcard)
		params[route.Wildcard[1:]] = path[begin:]
	} else if len(route.Params) > 0 {
		for _, param := range route.Params {
			start := strings.Index(route.Path, param)
			end := strings.Index(path[start:], "/")
			if end >= 0 {
				params[param[1:]] = path[start : start+end]
			} else {
				params[param[1:]] = path[start:]
			}
		}
	}
	return params
}

func (route *Route) GenerateMatcher(path string) string {
	if len(route.Params) > 0 {
		curr := 0
		le := len(route.Params) + 1
		parts := make([]string, le, le)
		for i, param := range route.Params {
			end := strings.Index(path, param)
			parts[i] = path[curr:end]
			curr = end + len(param)
		}
		parts[le-1] = path[curr:]
		return "^" + strings.Join(parts, "[^/]+") + "$"
	}
	if len(route.Wildcard) > 0 {
		return "^" + path[:strings.Index(path, route.Wildcard)] + ".+$"
	}
	return "^" + path + "$"
}

func (route *Route) MatchesRequest(r *http.Request) bool {
	if route.Method == r.Method {
		if matched, _ := regexp.MatchString(route.Matcher, r.URL.Path); matched {
			return true
		}
	}
	return false
}
