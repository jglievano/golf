package golf

import (
	"log"
	"time"
	"net/http"
)

type Router struct {
	routes []Route
}

func NewRouter() (router *Router) {
	router = &Router{routes: []Route{}}
	return
}

func (router *Router) handle(method, path string, handler func(*Handshake)) {
	route := NewRoute(method, path, handler)
	router.routes = extend(router.routes, *route)
}

func (router *Router) Get(path string, handler func(*Handshake)) {
	router.handle("GET", path, handler)
}

func (router *Router) Post(path string, handler func(*Handshake)) {
	router.handle("POST", path, handler)
}

func (router *Router) Static(path, root string) {
	// TODO: check there's no * or : in path.
	router.Get(path+"/*filepath", staticHandler(path, root))
}

func (router *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, route := range router.routes {
		if route.MatchesRequest(r) {
			hs := NewHandshake(w, r, route)
			hs.W.Header().Set("Cache-Control", "no-cache, private, max-age=0")
			hs.W.Header().Set("Expires", time.Unix(0, 0).Format(http.TimeFormat))
			hs.W.Header().Set("Pragma", "no-cache")
			hs.W.Header().Set("X-Accel-Expires", "0")
			log.Printf("%-6s %-25s %s\n", route.Method, route.Path, r.URL.Path)
			for k, v := range hs.Params {
				log.Printf("%7s%-15s %s", " ", k, v)
			}
			route.Handler(hs)
			return
		}
	}
}

func checkErr(err error) (erred bool) {
	if err != nil {
		log.Fatal(err)
		return true
	}
	return false
}

func extend(slice []Route, element Route) []Route {
	n := len(slice)
	if n == cap(slice) {
		newSlice := make([]Route, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

func staticHandler(path, root string) func(*Handshake) {
	fs := http.Dir(root)
	fileServer := http.StripPrefix(path, http.FileServer(fs))
	return func(c *Handshake) {
		fileServer.ServeHTTP(c.W, c.R)
	}
}
