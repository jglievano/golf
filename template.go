package golf

import (
	"html/template"
	"io"
)

var tmpl *template.Template

func ParseGlob(pattern string) error {
	var err error
	tmpl, err = template.ParseGlob(pattern)
	return err
}

func ExecuteTemplate(wr io.Writer, name string, data interface{}) error {
	err := tmpl.ExecuteTemplate(wr, name, data)
	return err
}
