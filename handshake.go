package golf

import "net/http"

type Handshake struct {
	R      *http.Request
	W      http.ResponseWriter
	Params map[string]string
}

func NewHandshake(w http.ResponseWriter, r *http.Request, route Route) *Handshake {
	return &Handshake{
		W:      w,
		R:      r,
		Params: route.PathParams(r.URL.Path),
	}
}

func (hs *Handshake) FormValue(key string) string {
	hs.R.ParseForm()
	value := hs.R.Form[key]
	if len(value) > 0 {
		return value[0]
	}
	return ""
}
